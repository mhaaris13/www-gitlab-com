---
layout: markdown_page
title: "FY23-Q3 OKRs"
description: "View GitLabs Objective-Key Results for FY23 Q3. Learn more here!"
canonical_path: "/company/okrs/fy23-q3/"
---

This [fiscal quarter](/handbook/finance/#fiscal-year) will run from August 1, 2022 to October 31, 2022.

Please note that Ally.io is the single source of truth for OKRs during the quarter. CEO OKRs should map to the CEO OKRs in Ally.io to the degree that all imformation on this page is public and what is in Ally.io can include internal only information.

## On this page
{:.no_toc}

- TOC
{:toc}

## OKR Schedule
The by-the-book schedule for the OKR timeline would be:

| OKR schedule | Week of | Task |
| ------ | ------ | ------ |
| -5 | 2022-06-27 | CEO shares top goals with E-group for feedback |
| -4 | 2022-07-04 | CEO pushes top goals to Ally.io |
| -4 | 2022-07-04 | E-group propose OKRs for their functions in the OKR draft review meeting agenda |
| -3 | 2022-07-11 | E-group 50 minute draft review meeting. After, function OKRs are put into Ally.io and links are shared in #okrs Slack channel  |
| -2 | 2022-07-18 | E-group discusses with their respective teams and polishes OKRs |
| -1 | 2022-07-25 | CEO reports post links to final OKRs in #okrs slack channel and @ mention the CEO and CoS to the CEO for approval |
| 0  | 2022-08-01 | CoS to the CEO updates OKR page for current quarter to be active and includes CEO level OKRs with consideration to what is public and non-public |


## OKRs

### 1. CEO 

### 2. CEO

### 3. CEO




